import { ref, computed } from 'vue'
import Home from "@/components/Home.vue";
import About from "@/components/About.vue";
import NotFound from "@/components/NotFound.vue";
import TalkingToApi from "@/components/TalkingToApiPublic.vue";
import TalkingToApiPrivate from "@/components/TalkingToApiPrivate.vue";

const routes = {
    '/': Home,
    '/about': About,
    '/public-api': TalkingToApi,
    '/private-api': TalkingToApiPrivate
}

const currentPath = ref(window.location.hash)

window.addEventListener('hashchange', () => {
    currentPath.value = window.location.hash
})

const currentView = computed(() => {
    return routes[currentPath.value.slice(1) || '/'] || NotFound
})

export { currentView }
